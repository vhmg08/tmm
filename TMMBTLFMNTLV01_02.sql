--#!/SQL/sh
--###########################################################################
--# Nombre del Programa : TMMBTLFMNTLV01_02.sql                             #
--# Autor               : oscar tavera anguiano                             #
--# Compania            : Tecnologia Actual en Software(TAS)                #
--# Proyecto/Procliente : Fecha: 20/07/2002                                 #
--# Descripcion General : Carga en la tranaux el archivo de duplicados 0420 #
--#                                                                         #
--# Programa Dependiente: N/A                                               #
--# Programa Subsecuente: N/A                                               #
--# Cond. de ejecucion  : N/A                                               #
--# Dias de ejecucion : 1 veces al dia Horario: 1:00 A.M                    #
--# M O D I F I C A C I O N E S                                             #
--#-------------------------------------------------------------------------#
--# Autor : Sonia Gonzalez Gonzalez                                         #
--# Compania : Grupo Gesfor-Tas(TAS)                                        #
--# Proyecto/Procliente : P-08-376-197758     Fecha: 21/11/2002             #
--# Modificacion : Se agregaron los siguientes campos a la tabla tbl_tranaux#
--#                - token_q4                                               #
--#                - token_q5                                               #
--#                - term_name                                              #
--#                - term_city                                              #
--#                - term_st                                                #
--#                - term_cntry                                             #
--# Modificacion : Integracion BSPR                                         #
--# Fecha : 02/07/2003                                                      #
--# Inicio: TASS C-002 Datos de quien realizo la modificacion(tokenQ25)     #
--# Autor : Sonia Gonzalez Gonzalez                                         #
--# Compania : Tecnologia Actual en Software(TAS)                           #
--# Proyecto/Procliente : PENDIENTE           Fecha: 08/08/2003             #
--# Modificacion : Se agrego un nuevo token(Q25)                            #
--# Fin   : TASS C-002                                                      #  
--#-------------------------------------------------------------------------#
--# Autor               : Oscar Tavera Anguiano                             #
--# Compania            : T y T Aplicaciones y Soluciones de Sistemas(TASS) #
--# Proyecto/Procliente : P-09-246-36476            Fecha: 27/09/2004       #
--# Modificacion        : Se agrega el concepto de multicortes              #
--#                                                                         #
--#-------------------------------------------------------------------------#
--# Autor               : Oscar Tavera Anguiano                             #
--# Compania            : T y T Aplicaciones y Soluciones de Sistemas(TASS) #
--# Proyecto/Procliente : P-09-093-06-178279        Fecha: 14/03/2006       #
--# Modificacion        : Se agrega el token pd (donativos)                 #
--#                                                                         #
--#-------------------------------------------------------------------------#
--# Autor               : Sonia Gonzalez Gonzalez                           #
--# Compania            : T y T Aplicaciones y Soluciones de Sistemas(TASS) #
--# Proyecto/Procliente : P-09-0916-08              Fecha: 03/10/2008       #
--# Modificacion        : Se agrega el token qt "consulta tarifa"(donativos)#
--#                                                                         #
--#-------------------------------------------------------------------------#
--# Numero de Parametros: 2                                                 #
--# Parametros Entrada : 1 Formato: FECHA(yyyymmddhh24mi)                   # 
--# Parametros Entrada : 2 Formato: 99                                      # 
--# Parametros Salida : 0 Formato: N/A                                      #
--#-------------------------------------------------------------------------#
--# Descripcion de Parametros:                                              #
--#                                                                         #
--# - PRM_FECHA_PROCESO = Es la Fecha que control-m, le pasara al shell,    #
--# en formato yyyymmdd                                                     #
--#                                                                         # 
--###########################################################################
LOAD DATA
 INFILE PRM_PATH_ARCHIVO
 APPEND 
 INTO TABLE TBL_TRANAUX
(
--# Modificacion        :  Marca de Inicio          TASS-OTA  P-09-246-36476  #
   fecha_carga               "TO_DATE(PRM_FECHA_PROCESOPRM_PARTE,'yyyymmddhh24mi')", 
   parte                      CONSTANT "0",
--# Modificacion        :  Marca de Terminacion     TASS-OTA  P-09-246-36476  #
   status_registro            CONSTANT "00",
   fh_origen_tran             POSITION (136:147)  DATE "YYMMDDHH24MISS",
   codigo_atm                 POSITION (42:51)   "NVL(:codigo_atm,'0000000000')",
   numero_cuenta_acceso       POSITION (66:84),
   numero_sec_trans           POSITION (156:167) "NVL(:numero_sec_trans,'000000000000')",
   codigo_tipo_mensaje        POSITION (90:93),
   folio_duplicado           "seq_tmm_orden.nextval",
   orden                     "seq_tmm_orden.nextval",
   cve_pais                   CONSTANT "00",
   moneda_cajero              POSITION (262:264) "NVL(:moneda_cajero,'000')",
   ban_codigo_banco           POSITION (62:65)   "NVL(:ban_codigo_banco,'0000')",
   codigo_banco               POSITION (38:41)   "NVL(:codigo_banco,'0000')",
   f_contable                 POSITION (150:155)  DATE "YYMMDD",
   tipo_transaccion           POSITION (28:29)   "NVL(:tipo_transaccion,'00')",
   clave_operacion            POSITION (170:171) "NVL(:clave_operacion,'00')",
   tip_codigo_tipo_cuenta     POSITION (172:173) "NVL(:tip_codigo_tipo_cuenta,'00')",
   codigo_tipo_cuenta         POSITION (174:175) "NVL(:codigo_tipo_cuenta,'00')",
   codigo_reversal            POSITION (306:307) "NVL(:codigo_reversal,'00')",
   retencion_tarjeta          POSITION (259:259) "NVL(:retencion_tarjeta,'0')",
   razon_respuesta            POSITION (260:261) "NVL(:razon_respuesta,'00')",
   numero_cuenta_origen       POSITION (176:194) "NVL(:numero_cuenta_origen,'0000000000000000000')",
   numero_cuenta_destino      POSITION (195:213) "NVL(:numero_cuenta_destino,'0000000000000000000')",
   cantidad_solicitada        POSITION (215:225) "NVL(:cantidad_solicitada,0)", 
   cantidad_balance           POSITION (226:236) "NVL(:cantidad_balance,0)",
   cantidad_disponible        POSITION (237:247) "NVL(:cantidad_disponible,0)",
   credito_deposito           POSITION (252:257) "NVL(:credito_deposito,0)",
   moneda_banco_propietario   position (276:278) "NVL(:moneda_banco_propietario,'000')",     
   moneda_balance_cliente     position (265:267) "NVL(:moneda_balance_cliente,'000')",     
   red_logica_cliente         POSITION (58:61)   "NVL(:red_logica_cliente,'0000')",
   red_logica_cajero          POSITION (34:37)   "NVL(:red_logica_cajero,'0000')",
   uso_sobre                  position (88:89)   "NVL(:uso_sobre,'00')",     
   origen_mensaje             POSITION (96:96)   "NVL(:origen_mensaje,'0')",
   origen_respuesta           POSITION (97:97)   "NVL(:origen_respuesta,'8')",
   status_mensaje             POSITION (94:95),
   proceso_autorizacion       POSITION (30:33)   "NVL(:proceso_autorizacion,'0000')",
   numero_miembro             position (85:87),     
   tipo_terminal              position (168:169),     
   codigo_seleccion_cuenta    POSITION (214:214),
   fh_exit                    position (117:130) DATE "yyyymmddhh24miss",     
   fh_dm_exit                 position (131:135),             
   fh_grabacion_log           POSITION (11:22)   DATE "YYMMDDHH24MISS",
   fh_dm_grabacion_log        position (23:27),             
   fh_entrada_b24             POSITION (98:111)  DATE "YYMMDDHH24MISS",
   fh_dm_entrada_b24          position (112:116),             
   factor_cambio_cliente      POSITION (268:275) "NVL(:factor_cambio_cliente,0)",
   factor_cambio_cajero       position (279:286),     
   fh_conversion_moneda      "TO_DATE(PRM_FECHA_PROCESO,'YYYYMMDD')",    
   semana                    "MOD(TO_NUMBER(TO_CHAR(TO_DATE(PRM_FECHA_PROCESO,'YYYYMMDD'),'IW')),13)",  
   tiempo_respuesta           CONSTANT "00",
   tipo_atm                   CONSTANT "0",
--# Modificacion        :  Marca de Inicio       GESFOR-SGG  P-08-376-197758  #
   auth_id                    POSITION(308:313),
   term_name                  POSITION(314:338),              
   term_city                  POSITION(339:351),              
   term_st                    POSITION(352:354),              
   term_cntry                 POSITION(355:356),              
   token_q4                   POSITION(371:408),              
   token_q5                   POSITION(409:522)
--# Modificacion        :  Marca de Terminacion  GESFOR-SGG  P-08-376-197758  #
--# Inicio : TASS C-002 Se agrega el token Q25(surcharge)                   #
   ,surcharge                 POSITION(533:602) "NVL(:surcharge,' ')"
--# Fin    : TASS C-002   
--# Modificacion        :  Marca de Inicio       TASS-OTA  P-09-093-06-178279 # 
   ,token_pd                  POSITION(603:660) "NVL(:token_pd,' ')"
--# Modificacion        :  Marca de Terminacion  TASS-OTA  P-09-093-06-178279 #
--# Modificacion        :  Marca de Inicio       TASS-SGG  P-09-0916-08       #
   ,token_qt                  POSITION(671:702) "NVL(:token_qt,' ')"
--# Modificacion        :  Marca de Terminacion  TASS-SGG  P-09-0916-08       #
)
