--#!/SQL/sh
--###########################################################################
--# Nombre del Programa : TMMBTLFMNTLV01_04.sql                             #
--# Autor               : oscar tavera anguiano                             #
--# Compania            : Tecnologia Actual en Software(TAS)                #
--# Proyecto/Procliente : Fecha: 20/07/2002                                 #
--# Descripcion General : Carga en la tbl_rechazos los registros que no se  #
--#                       pudieron cargar del extract                       #
--# Programa Dependiente: N/A                                               #
--# Programa Subsecuente: N/A                                               #
--# Cond. de ejecucion  : N/A                                               #
--# Dias de ejecucion : 1 veces al dia Horario: 1:00 A.M                    #
--# M O D I F I C A C I O N E S                                             #
--#-------------------------------------------------------------------------#
--# Autor               : Oscar Tavera Anguiano                             #
--# Compania            : T y T Aplicaciones y Soluciones de Sistemas(TASS) #
--# Proyecto/Procliente : P-09-246-36476            Fecha: 27/09/2004       #
--# Modificacion        : Se agrega el concepto de multicortes              #
--#                                                                         #
--#-------------------------------------------------------------------------#
--# Numero de Parametros: 1                                                 #
--# Parametros Entrada : 1 Formato: FECHA(yyyymmddhh24mi)                   # 
--# Parametros Salida : 0 Formato: N/A                                      #
--#-------------------------------------------------------------------------#
--# Descripcion de Parametros:                                              #
--#                                                                         #
--# - PRM_FECHA_PROCESO = Es la Fecha que control-m, le pasara al shell,    #
--# en formato yyyymmddhh24mi                                               #
--#                                                                         # 
--###########################################################################
--*/

LOAD DATA
 INFILE PRM_PATH_ARCHIVO
 APPEND 
 INTO TABLE TBL_RECHAZOS
(
--# Modificacion        :  Marca de Inicio          TASS-OTA  P-09-246-36476  #
   fecha_carga                "TO_DATE(PRM_FECHA_PROCESO,'yyyymmddhh24mi')", 
--# Modificacion        :  Marca de Terminacion     TASS-OTA  P-09-246-36476  #
   folio_rechazo              "seq_tmm_folio_rechazo.nextval",
   registro                   POSITION (1:317),
   status                     CONSTANT "0"
)
