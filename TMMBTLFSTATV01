#!/usr/bin/ksh
#############################################################################
# Nombre del Programa : TMMBTLFSTATV01                                      #
# Autor               : Sonia Gonzalez Gonzalez                             #
# Compania            : Grupo Gesfor-Tas                                    #
# Proyecto/Procliente :                               Fecha: 20/07/2002     #
# Descripcion General : Genera los archivos stat06 y stat07                 #
# Programa Dependiente: N/A                                                 #
# Programa Subsecuente: N/A                                                 #
# Cond. de ejecucion  : N/A                                                 #
# Dias de ejecucion   : Diario                    Horario: 1 vez al dia     #
#                       M O D I F I C A C I O N E S                         #
#---------------------------------------------------------------------------#
# Autor               : Felipe Aviles                                       #
# Compania            : TASS                                                #
# Proyecto/Procliente : N/A                       Fecha:  08/08/03          #
# Modificacion        : Adicion de surchange                                #
#---------------------------------------------------------------------------#
# Autor               : Sonia Gonzalez Gonzalez                             #
# Compania            : TASS                                                #
# Proyecto/Procliente : P-09-246-36476             Fecha:  15/03/04         #
# Modificacion        : Se genera un nuevo reporte de transacciones         #
#                       sospechosas                                         #
#---------------------------------------------------------------------------#
# Autor               : Sonia Gonzalez Gonzalez                             #
# Compania            : TASS                                                #
# Proyecto/Procliente : P-09-246-36476            Fecha:  06/10/04          #
# Modificacion        : Se adapta el shell para procesar varios cortes,     #
#                       procesos normales,reprocesos de un extract y        #
#                       reprocesos de todo 1 dia                            #
#---------------------------------------------------------------------------#
# Autor               : Sonia Gonzalez Gonzalez                             #
# Compania            : T y T Aplicaciones y Soluciones de Sistemas(TASS)   #
# Proyecto/Procliente : C-09-496-68268            Fecha: 22/11/2004         #
# Modificacion        : Se genera archivo de control de cifras detallado    #
#                       en base al banco,prefijo,num. registros e importe   #
# Numero de Parametros: 1                                                   #
# Parametros Entrada  : 1                                                   #
#                       - Fecha de Proceso      Formato: YYYYMMDD           #
# Parametros Salida   : N/A                       Formato: N/A              #
#---------------------------------------------------------------------------#
# Autor               : Sonia Gonzalez Gonzalez                             #
# Compania            : T y T Aplicaciones y Soluciones de Sistemas(TASS)   #
# Proyecto/Procliente : P-09-093-06-178279        Fecha: 18/03/2006         #
# Modificacion        : Se incorporan reversos, informacion MasterCard y    #
#                       se habilita la funcionalidad de donativos al pro-   #
#                       ceso de Cajeros de Banco Santander Colombia. Debido #
#                       a la reestructuracion de stats se eliminaron varias #
#                       lineas de inicio y fin de cambio ya que quedaban in-#
#                       conclusas por la gran cantidad de codigo que se eli-#
#                       mino(Ahora la ejecucion con surcharge es automatica)#
#---------------------------------------------------------------------------#
# Autor               : Carlos Mendez De Luna                               #
# Compania            : PROSA                                               #
# Proyecto/Procliente : C-04-2237-10              Fecha:  19/04/2010        #
# Modificacion        : Se comentan procesos para Santander PR (PR02)       #
# Marca del Cambio    : CML-C-04-2237-10                                    #
#---------------------------------------------------------------------------#
#Descripcion de parametros Fijos:                                           #
#   - PRM_PGM = Path donde se localiza el programa ejecutable               #
#   - PRM_INDICA_GENERA = Indica si genera reportes para varios bancos o    #
#                         para un banco en especifico VALORES(T o X)     #
#   - PRM_CVE_BANCO     = Clave del banco del cual se debe emitir el reporte#
#   - PRM_ARCHIVOS_ST06 = Path donde se se deberan alojar los archivos      #
#                         stat06 de cada banco                              #
#   - PRM_ARCHIVOS_ST07 = Path donde se se deberan alojar los archivos      #
#                         stat07 de cada banco                              #
#   - PRM_BITACORA      = Nombre del archivo bitacora de errores            #
#   - PRM_EQUIPO        = Clave de equipo donde se ejecuta el programa(DOMB)#
#                                                                           #
#---------------------------------------------------------------------------#
# Modificacion        :  Marca de Inicio       TASS-SGG  P-09-093-06-178279 #
PRM_HOME=/aplic/prod/tmm/tlf/
PRM_LOG=${PRM_HOME}log/
PRM_SAL=${PRM_HOME}sal/
PRM_SHE=${PRM_HOME}shell/
PRM_PGM=${PRM_HOME}pgmobj/
PRM_BCK=${PRM_HOME}ent/bck
PRM_FTE=${PRM_HOME}pgmfte/
PRM_ENT=${PRM_HOME}ent/
PRM_INDICA_GENERA=$2
PRM_CVE_BCO=$3
PRM_CVE_BCO1=H
PRM_ARCH_ST06=$PRM_SAL
PRM_ARCH_SURCH=${PRM_HOME}salh/
PRM_ARCH_DONAT=${PRM_HOME}sald/
PRM_ARCH_ST07=$PRM_SAL
PRM_FILE_LOG3=${PRM_LOG}TMMBTLFSTATV01.LOG3
PRM_TBL_TMMCTRL=tbl_tmmctrl
PRM_TBL_CATBANCO=tbl_catbanco

PRM_ARCH_SOSP=$PRM_SAL
PRM_BITACORA=$PRM_SAL/BITACORA.txt
PRM_EQUIPO=DOMB
V_MENS="N=Proceso normal,R=Reproceso de un extract,X=Reproceso de todo un dia, T=Genera los reportes de todos los bancos del dia indicado,B=Genera los reportes de un banco del dia indicado"
#***************************************************************************#
# PASO 01:                                                                  #
# valida que se reciba el parametro fecha en formato YYYYMMDD, el tipo de   #
# proceso adecuado y la clave de banco                                      #
#***************************************************************************#
if test -z "$1"
then
   echo "FALTA PARAMETRO FECHA DE PROCESO(FORMATO YYYYMMDD)"
   exit 2
fi

if test -z "$2"
then
   echo "FALTA PARAMETRO TIPO DE PROCESO($V_MENS)"
   exit 2
fi

if (test $2 != "N") && (test $2 != "R") && (test $2 != "X") && (test $2 != "T") && (test $2 != "B")
then
   echo "EL TIPO DE PROCESO DEBE SER=($V_MENS)"
   exit 2
fi

#***************************************************************************#
# PASO 02:                                                                  #
# Si el tipo de proceso es un "B" o una "H" se valida que se reciba el banco
#***************************************************************************#
if [ $2 = "B" ]
then
 if test -z "$3"
 then
   echo "FALTA PARAMETRO CLAVE DEL BANCO"
   exit 2
 fi
else
 PRM_CVE_BCO=X
fi

#***************************************************************************#
# PASO 03:                                                                  #
# Ejecuta el proceso TMMBTLFSTATV01_01(Genera los stat06 para los bcos adq) #
#***************************************************************************#
echo "----Ejecutando procesos (Genera los stat06 para los bcos adq.)...."
$PRM_PGM/TMMBTLFSTATV01_01 $1 $PRM_INDICA_GENERA $PRM_CVE_BCO $PRM_ARCH_ST06 $PRM_BITACORA $PRM_EQUIPO $PRM_ARCH_SURCH $PRM_ARCH_DONAT

#***************************************************************************#
# PASO 04:                                                                  #
# Verifica si el proceso termino mal                                        #
#***************************************************************************#
if [ $? -ne 0 ]
then
   echo "FALLO PROCESO TMMBTLFSTATV01_01(Genera los stat06 para los bcos adq.)"
   exit 2
fi

#***************************************************************************#
# PASO 05:                                                                  #
# Ejecuta el proceso TMMBTLFSTATV01_02(Genera los stat07 para los bcos emi) #
#***************************************************************************#
echo "----Ejecutando procesos (Genera los stat07 para los bcos emisores)...."
$PRM_PGM/TMMBTLFSTATV01_02 $1 $PRM_INDICA_GENERA $PRM_CVE_BCO $PRM_ARCH_ST07 $PRM_BITACORA $PRM_EQUIPO $PRM_ARCH_SURCH $PRM_ARCH_DONAT

#***************************************************************************#
# PASO 06:                                                                  #
# Verifica si el proceso termino mal                                        #
#***************************************************************************#
if [ $? -ne 0 ]
then
   echo "FALLO PROCESO TMMBTLFSTATV01_02(Genera los stat07 para los bcos emisores)"
   exit 2
fi

# Modificacion        :  Marca de Inicio      PROSA_CML  C-04-2237-10       #
#***************************************************************************#
# PASO 07:                                                                  #
# Ejecuta el proceso TMMBTLFSTATV01_03(Genera transacc. sospechosas)        #
#***************************************************************************#
#echo "----Ejecutando procesos (Genera transacciones sospechosas)...."
# PRM_CVE_BCO=PR02
#$PRM_PGM/TMMBTLFSTATV01_03 $1 $PRM_INDICA_GENERA $PRM_CVE_BCO $PRM_ARCH_SOSP $PRM_BITACORA $PRM_EQUIPO

#***************************************************************************#
# PASO 08:                                                                  #
# Verifica si el proceso termino mal                                        #
#***************************************************************************#
#if [ $? -ne 0 ]
#then
#   echo "FALLO PROCESO TMMBTLFSTATV01_03(Genera transacciones sospech.)"
#   exit 2
#fi
#
# Modificacion        :  Marca de Terminacion  TASS-SGG  P-09-093-06-178279 #
# Modificacion        :  Marca de Terminacion  PROSA_CML  C-04-2237-10      #

#---------------------------------------------------------------------------#
# fin del shell                                                             #
#---------------------------------------------------------------------------#
